# LEVEL 1: NEOPYTHE
# =================
#
# You have just begun your journey... practice and you will improve your skills
# and raise to new levels of magick.


# RUNNING TESTS
# =============
#
# To run tests for a level, use the test command followed by the level to test.
# The test command is available in the bin directory. To run tests for level 1
# you need to cd into the functions dir in your terminal and write: 
#
#   bin/test 1


# GRADING
# =======
# When you solved all the exercises and the tests pass, you need to send it for
# grading.
#
# Add a remote to the grading school by writing in your terminal:
#
#   git remote add grade ssh://spellbucks.patwic.com/<my-username>/functions.git
# 
# To submit your solotuins you need to push to the new grading school remote:
#
#   git push grade master:master
#
# The code you push will be tested and you will receive a mail with the results.
# If you pass all tests, you will get a password to the next level.
#
# NOTE: Don't forget that you have to add and commit your changes!


# HINTS
# =====
# If you have trouble understanding how to do, you can get some help by decoding
# the hints belonging to an exercise.
#
# The hints are named HINTnnn, where nnn is three-digit number. To decode a
# hint, use the showHint command:
#
#    bin/showHint HINT101
#
# This will decode HINT101 and show it in the terminal.


# START SOLVING THE EXERCISES
# ===========================
# Here are the exercises. Your job is to make the functions work as the
# docstrings indicate. Write code inside the function bodies and run the tests
# to see if your solution is correct. Good Luck!


# Like any other profession, wizards need to start modestly. Replace the pass
# statement below with a return statement with your rank. Your rank is
# 'Neopythe'.
#
# HINT111: R29vZCBqb2IsIHlvdW5nIG5lb3B5dGhlLiBZb3Ugbm93IGtub3cgaG93IHRvIGRlY29kZSBoaW50cyE=
# HINT112: VGhlICdyZXR1cm4nIGtleXdvcmQgZm9sbG93ZWQgYnkgeW91ciByYW5rLg==
# HINT113: VGhlIHN0cmluZyBtdXN0IG1hdGNoIGV4YWN0bHksIGJpZyBvciBzbWFsbCBsZXR0ZXJzIGZvciBleGFtcGxlLg==
def wizard_rank():
    """Returns your rank."""
    return "Neopythe"
         


# A wizard must know how to compose arcane chants. Declare the three variables
# needed in the return statement and give them corect values.
#
# HINT121: VmFyaWFibGVzIG11c3QgYmUgZGVjbGFyZWQgYmVmb3JlIHRoZXkncmUgdXNlZC4=
def arcane_chant():
    """Returns the arcane chant 'abra-kadabra-kazam'."""
    a= "abra"
    b= "kadabra"
    c= "kazam"
    
    # Declare the three variables abra, kadabra, and kazam here...
    
    return a + "-" + b + "-" + c


# Sometimes words have to be repeated.
#
# HINT131: NyBhYnJhLXMgZm9sbG93ZWQgYnkgYSBrYWRhYnJh
# HINT132: V2hhdCBoYXBwZW5zIGlmIGEgc3RyaW5nIGlzIG11bHRpcGxpZWQgd2l0aCBhbiBpbnRlZ2VyPw==
def repetitive_chant():
    """Return the string 'abra-abra-abra-abra-abra-abra-abra-kadabra'."""
    return "abra-" * 7 + "kadabra"
    

# Just as 0 means "nothing" in matehematics, there is a concept of nothing in
# python programming. It's called 'None'.
#
# HINT141: SnVzdCByZXR1cm4gTm9uZQ==
def nothing():
    """Returns None."""
    return None 

